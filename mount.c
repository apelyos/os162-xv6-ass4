#include "types.h"
#include "user.h"

int
main(int argc, char *argv[])
{
  int res;

  if(argc != 3){
    printf(1, "incorrect arg count.\n");
    exit();
  }

  res = mount(argv[1], atoi(argv[2]));

  if (res == 0)
    printf(1, "Mount success.\n");
  else
    printf(1, "Mount FAILED!\n");

  exit();
}
