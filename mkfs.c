#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <assert.h>

#define stat xv6_stat  // avoid clash with host struct stat
#include "types.h"
#include "fs.h"
#include "stat.h"
#include "param.h"
#include "mbr.h"

#ifndef static_assert
#define static_assert(a, b) do { switch (0) case 0: case (a): ; } while (0)
#endif

#define NINODES 200
#define BEGIN_OF_PARTITION 1 // start block of 1st partition
uint offset = 1;

// Disk layout:
// [ boot block | sb block | log | inode blocks | free bit map | data blocks ]

int nbitmap = FSSIZE/(BSIZE*8) + 1;
int ninodeblocks = NINODES / IPB + 1;
int nlog = LOGSIZE;  
int nmeta;    // Number of meta blocks (boot, sb, nlog, inode, bitmap)
int nblocks;  // Number of data blocks

int fsfd;
struct superblock sb;
char zeroes[BSIZE];
uint freeinode = 1;
uint freeblock;


void balloc(int);
void wsect(uint, void*);
void winode(uint, struct dinode*);
void rinode(uint inum, struct dinode *ip);
void rsect(uint sec, void *buf);
uint ialloc(ushort type);
void iappend(uint inum, void *p, int n);

// convert to intel byte order
ushort
xshort(ushort x)
{
  ushort y;
  uchar *a = (uchar*)&y;
  a[0] = x;
  a[1] = x >> 8;
  return y;
}

uint
xint(uint x)
{
  uint y;
  uchar *a = (uchar*)&y;
  a[0] = x;
  a[1] = x >> 8;
  a[2] = x >> 16;
  a[3] = x >> 24;
  return y;
}

struct dpartition create_partition(int argc, char *argv[] , uint bstart){
	  struct dpartition partition;
	  int i, cc, fd;
	  uint rootino, inum, off;
	  char buf[BSIZE];
	  struct dinode din;
	  struct dirent de;
	  int change_name;


	  printf("partition starts: %d\n", bstart);
	  offset = bstart;
	  // 1 fs block = 1 disk sector
	  nmeta = 1 + nlog + ninodeblocks + nbitmap;
	  nblocks = FSSIZE - nmeta;
	  freeinode = 1;

	  sb.size = xint(FSSIZE);
	  sb.nblocks = xint(nblocks);
	  sb.ninodes = xint(NINODES);
	  sb.nlog = xint(nlog);
	  sb.logstart = xint(1);
	  sb.inodestart = xint(1+nlog);
	  sb.bmapstart = xint(1+nlog+ninodeblocks);

	  printf("nmeta %d (boot, super, log blocks %u inode blocks %u, bitmap blocks %u) blocks %d total %d\n",
	         nmeta, nlog, ninodeblocks, nbitmap, nblocks, FSSIZE);

	  freeblock = nmeta + bstart;     // the first free block that we can allocate

	  for(i = offset; i < (FSSIZE + offset); i++)
	    wsect(i, zeroes);



	  memset(buf, 0, sizeof(buf));
	  memmove(buf, &sb, sizeof(sb));
	  wsect(offset, buf);

	  rootino = ialloc(T_DIR);
	  assert(rootino == ROOTINO);

	  bzero(&de, sizeof(de));
	  de.inum = xshort(rootino);
	  strcpy(de.name, ".");
	  iappend(rootino, &de, sizeof(de));

	  bzero(&de, sizeof(de));
	  de.inum = xshort(rootino);
	  strcpy(de.name, "..");
	  iappend(rootino, &de, sizeof(de));

	  for(i = 4; i < argc; i++){
	    assert(index(argv[i], '/') == 0);
	    if((fd = open(argv[i], 0)) < 0){
	      perror(argv[i]);
	      exit(1);
	    }
	    change_name = 0;
	    // Skip leading _ in name when writing to file system.
	    // The binaries are named _rm, _cat, etc. to keep the
	    // build operating system from trying to execute them
	    // in place of system binaries like rm and cat.
	    if(argv[i][0] == '_'){
	      ++argv[i];
	      change_name = 1;
	    }
	    inum = ialloc(T_FILE);

	    bzero(&de, sizeof(de));
	    de.inum = xshort(inum);
	    strncpy(de.name, argv[i], DIRSIZ);
	    iappend(rootino, &de, sizeof(de));

	    if (change_name)
	      --argv[i]; // For next time

	    while((cc = read(fd, buf, sizeof(buf))) > 0)
	      iappend(inum, buf, cc);

	    close(fd);
	  }

	  // fix size of root inode dir
	  rinode(rootino, &din);
	  off = xint(din.size);
	  off = ((off/BSIZE) + 1) * BSIZE;
	  din.size = xint(off);
	  winode(rootino, &din);

	  balloc(freeblock - offset);

	  partition.offset = bstart; // ??? 512
	  partition.flags = 0 | PART_ALLOCATED | PART_BOOTABLE; // OK to create all partitions as bootable???
	  partition.type = 0 | FS_INODE; //???
	  partition.size = FSSIZE; // in blocks

	  return partition;
}

int
main(int argc, char *argv[])
{
  int fd;
  char buf[BSIZE];
  struct mbr mbr;
  uint block_index, i;


  static_assert(sizeof(int) == 4, "Integers must be 4 bytes!");

  if(argc < 2){
    fprintf(stderr, "Usage: mkfs fs.img files...\n");
    exit(1);
  }

  assert((BSIZE % sizeof(struct dinode)) == 0);
  assert((BSIZE % sizeof(struct dirent)) == 0);

  fsfd = open(argv[1], O_RDWR|O_CREAT|O_TRUNC, 0666);
  if(fsfd < 0){
    perror(argv[1]);
    exit(1);
  }

  //read kernel
  block_index = 1;
  fd = open(argv[2], O_RDWR);
  while(read(fd, buf, BSIZE)){
	  wsect(block_index, buf);
	  ++block_index;
  }
  close(fd);

  //read bootloader;
  memset(mbr.partitions, 0 , sizeof(mbr.partitions));
  fd = open(argv[3], O_RDWR);
  read(fd, buf, BSIZE);
  memcpy(&mbr, buf, sizeof(mbr));
  close(fd);

  // Write MBR here
  for(i = 0; i < NPARTITIONS ; ++i)
	  mbr.partitions[i] = create_partition(argc, argv, block_index+FSSIZE*i);
  memset(buf, 0, BSIZE);
  memcpy(buf, &mbr, sizeof(mbr));
  wsect(0, buf);

  close(fsfd);

  exit(0);
}

void
wsect(uint sec, void *buf)
{
  if(lseek(fsfd, sec * BSIZE, 0) != sec * BSIZE){
    perror("lseek");
    exit(1);
  }
  if(write(fsfd, buf, BSIZE) != BSIZE){
    perror("write");
    exit(1);
  }
}

void
winode(uint inum, struct dinode *ip)
{
  char buf[BSIZE];
  uint bn;
  struct dinode *dip;

  bn = IBLOCK(inum, sb) + offset;
  rsect(bn, buf);
  dip = ((struct dinode*)buf) + (inum % IPB);
  *dip = *ip;
  wsect(bn, buf);
}

void
rinode(uint inum, struct dinode *ip)
{
  char buf[BSIZE];
  uint bn;
  struct dinode *dip;

  bn = IBLOCK(inum, sb) + offset;
  rsect(bn, buf);
  dip = ((struct dinode*)buf) + (inum % IPB);
  *ip = *dip;
}

void
rsect(uint sec, void *buf)
{
  if(lseek(fsfd, sec * BSIZE, 0) != sec * BSIZE){
    perror("lseek");
    exit(1);
  }
  if(read(fsfd, buf, BSIZE) != BSIZE){
    perror("read");
    exit(1);
  }
}
//alloc inodes
uint
ialloc(ushort type)
{
  uint inum = freeinode++;
  struct dinode din;

  bzero(&din, sizeof(din));
  din.type = xshort(type);
  din.nlink = xshort(1);
  din.size = xint(0);
  winode(inum, &din);
  return inum;
}

void
balloc(int used)
{
  uchar buf[BSIZE];
  int i;

  printf("balloc: first %d blocks have been allocated\n", used);
  assert(used < BSIZE*8);
  bzero(buf, BSIZE);
  for(i = 0; i < used; i++){
    buf[i/8] = buf[i/8] | (0x1 << (i%8));
  }
  printf("balloc: write bitmap block at sector %d\n", sb.bmapstart);
  wsect(sb.bmapstart + offset, buf);
}

#define min(a, b) ((a) < (b) ? (a) : (b))

void
iappend(uint inum, void *xp, int n)
{
  char *p = (char*)xp;
  uint fbn, off, n1;
  struct dinode din;
  char buf[BSIZE];
  uint indirect[NINDIRECT];
  uint x;

  rinode(inum, &din);
  off = xint(din.size);
  // printf("append inum %d at off %d sz %d\n", inum, off, n);
  while(n > 0){
    fbn = off / BSIZE;
    assert(fbn < MAXFILE);
    if(fbn < NDIRECT){
      if(xint(din.addrs[fbn]) == 0){
        din.addrs[fbn] = xint(freeblock++ - offset);
      }
      x = xint(din.addrs[fbn]) + offset;
    } else {
      if(xint(din.addrs[NDIRECT]) == 0){
        din.addrs[NDIRECT] = xint(freeblock++ - offset);
      }
      rsect(xint(din.addrs[NDIRECT]) + offset, (char*)indirect);
      if(indirect[fbn - NDIRECT] == 0){
        indirect[fbn - NDIRECT] = xint(freeblock++ - offset);
        wsect(xint(din.addrs[NDIRECT]) + offset, (char*)indirect);
      }
      x = xint(indirect[fbn-NDIRECT]) + offset;
    }
    n1 = min(n, (fbn + 1) * BSIZE - off);
    rsect(x, buf);
    bcopy(p, buf + off - (fbn * BSIZE), n1);
    wsect(x, buf);
    n -= n1;
    off += n1;
    p += n1;
  }
  din.size = xint(off);
  winode(inum, &din);
}
